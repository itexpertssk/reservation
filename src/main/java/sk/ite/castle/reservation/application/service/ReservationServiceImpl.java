package sk.ite.castle.reservation.application.service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


import org.modelmapper.ModelMapper;
import org.slf4j.Logger;

import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.ite.castle.reservation.application.dto.DTOReservation;
import sk.ite.castle.reservation.domain.model.ResPeriod;
import sk.ite.castle.reservation.infrastructure.persistence.ReservationRepository;
import sk.ite.castle.reservation.infrastructure.persistence.ResPeriodRepository;
import sk.ite.castle.reservation.domain.model.Reservation;
import sk.ite.castle.reservation.domain.model.Guest;

@Service
public class ReservationServiceImpl implements ReservationService {
	final Logger LOG = LoggerFactory.getLogger(ReservationServiceImpl.class);

	@Autowired
	private ReservationRepository reservationRepository;
	@Autowired
	private ResPeriodRepository resPeriodRepository;
	@Autowired
	ModelMapper dtoMapper;

	@Override
	public Long createReservationPeriod(Long castleId, Date from, Date to, float priceForVisitor, int maxNumberOfVisitors) {
		ResPeriod resPeriod = new ResPeriod(castleId,from,to,priceForVisitor,maxNumberOfVisitors);
		resPeriodRepository.save(resPeriod);
		return resPeriod.getId();
	}

	@Override
	public Long createReservation(Long customerId, Long periodId, String note) {
		Reservation newBooking = new Reservation(customerId,periodId,note);
		reservationRepository.save(newBooking);
		return newBooking.getId();
	}

	@Override
	public void addGuestToReservation(Long bookingId, String name, String surname, Long discountCardId) {
		Reservation booking= reservationRepository.getOne(bookingId);
		booking.addGuest(new Guest(name,surname,discountCardId));
		reservationRepository.save(booking);
	}

	@Override
	public void cancelReservation(long bookingId) {
		Reservation booking= reservationRepository.getOne(bookingId);
		booking.cancel();
	}

	@Override
	public List<DTOReservation> getAllReservationsForCustomer(Long customerId) {
		Collection<Reservation> byCustomerId = reservationRepository.getByCustomerId(customerId);
		return StreamSupport.stream(byCustomerId.spliterator(), false)
				.map(p -> dtoMapper.map(p, DTOReservation.class))
				.collect(Collectors.toList());
	}
}
