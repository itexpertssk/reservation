package sk.ite.castle.reservation.application.dto;

public class DTODiscountCard {
	public Long id;
	public String name;
	public Integer discountInPercentage;
}
