package sk.ite.castle.reservation.application.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sk.ite.castle.reservation.application.dto.DTOReservation;
import sk.ite.castle.reservation.application.service.ReservationService;

import java.util.List;

/**
 * Created by macalaki on 31.01.2017.
 */
@RestController
public class ReservationRestController {
    final Logger LOGGER = LoggerFactory.getLogger(ReservationRestController.class);

    @Autowired
    ReservationService reservationService;

    @RequestMapping(produces={"application/json"}, value="/reservations/customer/{customerId}", method= RequestMethod.GET )
    public List<DTOReservation> getAllReservationsForCustomer(@PathVariable Long customerId) {
        //TODO implementation
        LOGGER.info("Getting all Reservations for customer ...");
        List<DTOReservation> allReservationsForCustomer = reservationService.getAllReservationsForCustomer(customerId);
        return allReservationsForCustomer;
    }
}
