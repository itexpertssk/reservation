package sk.ite.castle.reservation.infrastructure.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sk.ite.castle.reservation.domain.model.Castle;

@Repository
public interface CastleRepository extends JpaRepository<Castle,Long> {

}
