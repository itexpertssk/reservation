package sk.ite.castle.reservation.infrastructure.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sk.ite.castle.reservation.domain.model.Reservation;

import java.util.Collection;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation,Long> {

    Collection<Reservation> getByCustomerId(Long customerId);
}

