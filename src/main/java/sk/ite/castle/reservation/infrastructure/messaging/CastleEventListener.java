package sk.ite.castle.reservation.infrastructure.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import sk.ite.castle.castle.domain.event.CastleCreatedEvent;
import sk.ite.castle.reservation.domain.model.Castle;
import sk.ite.castle.reservation.infrastructure.persistence.CastleRepository;

import java.util.function.Consumer;

@SpringBootConfiguration
public class CastleEventListener {
    final Logger LOG = LoggerFactory.getLogger(CastleEventListener.class);

    @Autowired
    CastleRepository castleRepository;

    @Bean
    public Consumer<CastleCreatedEvent> castlesink(){
        return (event) -> {
            LOG.info("Event received " + event);
            Castle castleToAdd = new Castle(event.getId(), event.getName());
            castleRepository.save(castleToAdd);
            };
    }
}
