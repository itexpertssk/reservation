package sk.ite.castle.reservation.infrastructure.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sk.ite.castle.reservation.domain.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {
}
