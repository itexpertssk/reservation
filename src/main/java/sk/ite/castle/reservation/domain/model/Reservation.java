package sk.ite.castle.reservation.domain.model;

import jakarta.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

// AggregateRoot
@Entity
public class Reservation {
	private @Id
	@GeneratedValue Long id;
	private @Version Long version;
	private Long customerId;
	private Long periodId;
	private String note;
	private Date lastModified;
	private Date createAt;
	private Date cancelAt;
	private Integer numberOfGuests = 0;
	private Float totalPrice = 0f;
	@ElementCollection
	@CollectionTable(
			name="GUEST",
			joinColumns=@JoinColumn(name="BOOKING_ID")
	)
	private Collection<Guest> guests = new HashSet<Guest>();

	public Reservation() {
	}

	public Reservation(Long customerId, Long periodId, String note) {
		this.customerId = customerId;
		this.periodId = periodId;
		this.note = note;

		createAt = new Date();
		lastModified = createAt;
	}

	public void addGuest(Guest guest){
		guests.add(guest);
		numberOfGuests++;
	}

	public void cancel(){
		cancelAt = new Date();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getPeriodId() {
		return periodId;
	}

	public void setPeriodId(Long periodId) {
		this.periodId = periodId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Date getCancelAt() {
		return cancelAt;
	}

	public void setCancelAt(Date cancelAt) {
		this.cancelAt = cancelAt;
	}

	public Integer getNumberOfGuests() {
		return numberOfGuests;
	}

	public void setNumberOfGuests(Integer numberOfGuests) {
		this.numberOfGuests = numberOfGuests;
	}

	public Float getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Float totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Collection<Guest> getGuests() {
		return guests;
	}

	public void setGuests(Collection<Guest> guests) {
		this.guests = guests;
	}
}
