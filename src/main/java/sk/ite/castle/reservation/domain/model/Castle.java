package sk.ite.castle.reservation.domain.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Castle {
	private @Id Long id;
	private String name;

	public Castle() {
	}

	public Castle(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
