package sk.ite.castle.reservation.domain.model;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Version;

import java.util.Date;

@Entity
public class ResPeriod {
	private @Id
	@GeneratedValue Long id;
	private @Version Long version;
	private Long castleId;
	private Date dateFrom;
	private Date dateTo;
	private float priceForVisitor;
	private int numberOfVisitors;
	private int maxNumberOfVisitors;
	private Date canceledAt;
	private Date createdAt;
	private boolean finished;
	private String program;

	public ResPeriod() {
	}

	public ResPeriod(Long castleId, Date from, Date to, float priceForVisitor, int maxNumberOfVisitors) {
		this.castleId = castleId;
		this.dateFrom = from;
		this.dateTo = to;
		this.priceForVisitor = priceForVisitor;
		this.maxNumberOfVisitors = maxNumberOfVisitors;
	}

	public Long getId() {
		return id;
	}
}
