package sk.ite.castle.reservation.domain.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Version;

@Entity
public class DiscountCard {
	private @Id
	@GeneratedValue Long id;
	private @Version Long version;
	private String name;
	private Integer discountInPercentage;

	public DiscountCard() {
	}

	public DiscountCard(String name, Integer discountInPercentage) {
		this.name = name;
		this.discountInPercentage = discountInPercentage;
	}

	public String getName() {
		return name;
	}

	public Integer getDiscountInPercentage() {
		return discountInPercentage;
	}
}
