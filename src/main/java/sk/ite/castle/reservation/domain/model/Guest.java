package sk.ite.castle.reservation.domain.model;

import jakarta.persistence.Embeddable;

@Embeddable
public class Guest {
	private String name = null;
	private String surname = null;
	private Long  discountCardId;

	public Guest(String name, String surname, Long discountCardId) {
		this.name = name;
		this.surname = surname;
		this.discountCardId = discountCardId;
	}

	// Required by modelmapper
	public Guest() {
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public Long getDiscountCardId() {
		return discountCardId;
	}
}
