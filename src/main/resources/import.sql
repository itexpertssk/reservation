insert into CASTLE (ID, NAME) values (1,'Harrenhal');
insert into CASTLE (ID, NAME) values (2,'Dragonstone');
insert into CASTLE (ID, NAME) values (3,'Casterly Rock');
insert into CASTLE (ID, NAME) values (4,'Winterfell');
insert into CASTLE (ID, NAME) values (5,'Riverrun');
insert into CASTLE (ID, NAME) values (6,'The Eyrie');
insert into CASTLE (ID, NAME) values (7,'Castle Black');
insert into CASTLE (ID, NAME) values (8,'Moat Cailin');
insert into CASTLE (ID, NAME) values (9,'The Dreadfort');
insert into CASTLE (ID, NAME) values (10,'Red Keep');
insert into CASTLE (ID, NAME) values (11,'Storm''s End');
insert into CASTLE (ID, NAME) values (12,'Highgarden');
insert into CASTLE (ID, NAME) values (14,'The Twins');
insert into CASTLE (ID, NAME) values (15,'Last Hearth');
insert into CASTLE (ID, NAME) values (16,'The Citadel');
insert into CASTLE (ID, NAME) values (17,'The Nightfort');

-- Registered Customers
insert into CUSTOMER (ID, EMAIL, NAME, SURNAME, VERSION) values (1, 'john.snow@got.net' , 'John', 'Snow', 1)
insert into CUSTOMER (ID, EMAIL, NAME, SURNAME, VERSION) values (2, 'arya.stark@got.net' , 'Arya', 'Stark', 1)

-- Example PERIOD
insert into RES_PERIOD (ID, CASTLE_ID, CANCELED_AT, CREATED_AT, DATE_FROM, DATE_TO, FINISHED, MAX_NUMBER_OF_VISITORS, NUMBER_OF_VISITORS, PRICE_FOR_VISITOR, PROGRAM, VERSION ) values(1, 1, NULL, '2019-01-01', '2019-01-01', '2019-01-02', FALSE, 25, 0, 5.50, 'Basic',1);

-- Example Reservation for PERIOD ID=1
insert into RESERVATION (ID, CANCEL_AT, CREATE_AT, CUSTOMER_ID, LAST_MODIFIED, NOTE, NUMBER_OF_GUESTS, PERIOD_ID, TOTAL_PRICE, VERSION ) values (1, NULL, '2018-12-10', 1, '2018-12-10', 'First visit', 3, 1, 16.50, 1);

-- Guests for RESERVATION ID=1
insert into GUEST (BOOKING_ID, DISCOUNT_CARD_ID, NAME, SURNAME) values (1, NULL, 'John', 'Snow');
insert into GUEST (BOOKING_ID, DISCOUNT_CARD_ID, NAME, SURNAME) values (1, NULL, 'Eddard', 'Stark');
insert into GUEST (BOOKING_ID, DISCOUNT_CARD_ID, NAME, SURNAME) values (1, NULL, 'Robb', 'Stark');


